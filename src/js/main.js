import $ from "jquery";
import {TweenMax} from 'gsap';
import ScrollToPlugin from "gsap/ScrollToPlugin";
import "./scrollify.js";
import './custom-slider.js';


$(document).ready(function () {
		
	//fullpage scroll
	$.scrollify({
		section: '.section',
		easing: "easeOutExpo",
		scrollSpeed: 1500,
	})

	//services sidebar trigger
	$('.services__list-item-title').on('click', function (e) {
		
		let parent = $(this).closest('.services__list-item');

		let obj = parent.find('.services__sublist');
		obj.css('display', 'flex');
		
		let tm = TweenMax.fromTo(obj, .4, {x: -1000}, {x: 0})
	})

	$('.sublist__close').on('click', function () {
		let list = $(this).closest('.services__list-item');
		let sublist = list.find('.sublist');
		let tm = TweenMax.to(sublist, .4, {x: -700});
		tm.eventCallback('onComplete', function () {
			sublist.css('display', 'none');
		})
	})


	$('.sublist__close').on('mouseenter', function () {
		let tm = TweenMax.to( $(this), 1, {rotation: 90, ease: Elastic.easeOut.config(1, 0.3)} );
	})

	$('.sublist__close').on('mouseleave', function () {
		let tm = TweenMax.to( $(this), 1, {rotation: 0, ease: Elastic.easeOut.config(1, 0.3)} );
	})





	//product-page => fix contacts & topline fields
	$(window).scroll(function () {
		let winTop = $(window).scrollTop();
		let prodAbs = $('.js-prod-start').offset().top;

		let prodTop = prodAbs - winTop;
		if (prodTop <= 0) {
			$('.contacts').css('position', 'fixed');
			$('.contacts').css('z-index', '999');

			$('.js-topline').addClass('fixed');
			$('.js-category-name').addClass('fix-mode');

		} else {
			$('.contacts').css('position', 'absolute');
			$('.js-topline').removeClass('fixed');
			$('.js-category-name').removeClass('fix-mode');
		}
	})

	//mobile-menu trigger
	$('.js-mobile-menu-trigger').on('mouseup', function () {
		$(this).toggleClass('menu-btn_active');
		$('.js-mobile-menu-content').slideToggle(0);
		$('.js-top').toggleClass('mobile-bg')
	})

})