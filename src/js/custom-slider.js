import $ from "jquery";
import {TweenMax} from 'gsap';
import ScrollToPlugin from "gsap/ScrollToPlugin";


/**************
*** SLIDER ****
**************/

$(document).ready(function () {
	$('.slider__wrapper').data('slidecount', ( $('.slider__wrapper').children().length ) )
	
	//number formatting (example: from 6 to 06)
	function getStringNumber(value) {
		if (String(value).length < 2) {
			return '0' + value;
		} else {
			return '' + value;
		}
	}

	let slidecount = $('.slider__wrapper').data('slidecount');
	showInfo($('.slider__slide.active').data('name'), $('.slider__slide.active').data('numindex') ,getStringNumber(slidecount))




	function showInfo(value, numIndex, numCount) {
		$('.gallery-title').text(value);
		$('.js-img-index').text(numIndex);
		$('.js-img-count').text(getStringNumber(numCount));
	}
	

	$('.js-slider-btn').on('mouseup', function () {
		let sliderWrapper 			= $('.slider__wrapper'),
			slides 					= sliderWrapper.find('.slider__slide'),
			activeSlide 			= sliderWrapper.find('.active'),
			existAfterNextSlide 	= activeSlide.next().next().next().length > 0,
			currentPos				= sliderWrapper.scrollTop(),
			direction				= $(this).data('dir')


			let scrollValue;

			if (direction == 'next') {
				scrollValue = currentPos + activeSlide.next().position().top;
			} else {
				scrollValue = currentPos + activeSlide.prev().position().top;
			}


			let tm = TweenMax.to(sliderWrapper, .7, {scrollTo: scrollValue});
				tm.eventCallback("onComplete", function () {

					activeSlide.removeClass('active')


					if (direction == 'next') {
						let currentSlide = activeSlide.next();
						currentSlide.addClass('active')
						
						if (currentSlide.next().next().length < 1) {

							let cloneIndex = sliderWrapper.data('cloneindex');
							
							let cloned = slides.eq(cloneIndex).clone();
							sliderWrapper.data('cloneindex', cloneIndex += 1);
							sliderWrapper.append(cloned.addClass('cloned'));
						}
						showInfo(currentSlide.data('name'), currentSlide.data('numindex'), sliderWrapper.data('slidecount'))

					} else {
						let currentSlide = activeSlide.prev()
						currentSlide.addClass('active')
						if (!currentSlide.next().next().hasClass('cloned')) {
							$('.cloned').remove()
							sliderWrapper.data('cloneindex', 0);
						}
						showInfo(currentSlide.data('name'), currentSlide.data('numindex'), sliderWrapper.data('slidecount'))
					}

					

					//hide-show button
					if (sliderWrapper.scrollTop() > 0) {
						$('.slider__prev').removeClass('hidden')
					} else {
						$('.slider__prev').addClass('hidden');
					}
				})
	})

	
})


